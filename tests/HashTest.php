<?php

use App\Models\User;
use Laravel\Lumen\Testing\DatabaseMigrations;
use Laravel\Lumen\Testing\DatabaseTransactions;
use Tymon\JWTAuth\Facades\JWTAuth;

/**
 * #### Class Login ####
 *
 * Hash Tests.
 *
 */
class HashTest extends TestCase
{
    protected $user;

    /**
     * Get Hash data with login user
     */
    public function testHashGet()
    {
        //Clean Any Test User;
        User::where('email', $this->testEmail)->forceDelete();
        //Create Test User;
        $user = $this->createTestUser();

        //This should login
        $token = JWTAuth::fromUser($user);
        $request = $this->get('/hash?token=' . $token);
        $request->assertResponseStatus(200);
        $request->seeJsonStructure(['hash']);
    }

    /**
     * Hash Test without valid token;
     */
    public function testHashFailWithoutValidToken()
    {
        //Clean Any Test User;
        User::where('email', $this->testEmail)->forceDelete();
        //Create Test User;
        $user = $this->createTestUser();


        $request = $this->get('/hash');
        $request->assertResponseStatus(401);
        $request->seeJson([
            'message' => 'UNAUTHORIZED',
        ]);
    }


}
