<?php

use App\Models\User;
use Laravel\Lumen\Testing\DatabaseMigrations;
use Laravel\Lumen\Testing\DatabaseTransactions;

/**
 * #### Class Login ####
 *
 * Login Tests.
 *
 */
class LoginTest extends TestCase
{

    /**
     * User Login Test with Correct Data
     */
    public function testLoginsSuccessfully()
    {
        //Clean Any Test User;
        User::where('email', $this->testEmail)->forceDelete();
        //Create Test User;
        $this->createTestUser();

        //This should login
        $request = $this->post('/login', ['email' => $this->testEmail, 'password' => '12345678']);
        $request->assertResponseStatus(200);
        $request->seeJsonStructure(['access_token']);
    }

    /**
     * User Login Test with emtpy or malformed parameters
     */
    public function testLoginFailsWithEmptyOrMalformedParameters()
    {
        //Clean Any Test User;
        User::where('email', $this->testEmail)->forceDelete();
        //Create Test User;
        $this->createTestUser();

        //This should fail with 422 and validation errors because email is empty
        $request = $this->post('/login', ['email' => '', 'password' => '12345678']);
        $request->assertResponseStatus(422);
        $request->seeJsonStructure(['email']);

        //User forget @ in email address
        $request = $this->post('/login', ['email' => 'testtest.com', 'password' => '12345678']);
        $request->assertResponseStatus(422);
        $request->seeJsonStructure(['email']);

        //User forget to type a password
        $request = $this->post('/login', ['email' => $this->testEmail, 'password' => '']);
        $request->assertResponseStatus(422);
        $request->seeJsonStructure(['password']);
    }

    /**
     * User Login Test with a wrong password
     */
    public function testLoginFailsWithWrongPassword()
    {
        //Clean Any Test User;
        User::where('email', $this->testEmail)->forceDelete();
        //Create Test User;
        $this->createTestUser();

        //This should fail with validation error because password confirmation mismatchs.
        $request = $this->post('/login', ['email' => $this->testEmail, 'password' => '12345678910']);
        $request->assertResponseStatus(401);
        $request->seeJson([
            'message' => 'UNAUTHORIZED',
        ]);
    }

    /**
     *User Login Test with a password that cant match with requirements
     */
    public function testLoginFailsWithTooShortPasswords()
    {
        //Clean Any Test User;
        User::where('email', $this->testEmail)->forceDelete();
        //This should fail with validation error on password because password isnt match with requirements of minimum 8 character.
        $request = $this->post('/login', ['email' => $this->testEmail, 'password' => '123456']);
        $request->assertResponseStatus(422);
        $request->seeJsonStructure(['password']);
    }

}
