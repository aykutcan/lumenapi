<?php

use App\Models\User;
use Laravel\Lumen\Testing\DatabaseMigrations;
use Laravel\Lumen\Testing\DatabaseTransactions;

/**
 * #### Class RegisterTest ####
 *
 * Register Page Testings.
 *
 */
class RegisterTest extends TestCase
{
    /**
     * User Register Test with Correct Data
     */
    public function testRegistersUserSuccessfully()
    {
        //Clean Any Test User;
        User::where('email', $this->testEmail)->forceDelete();
        //This should create user without error.
        $request = $this->post('/register', ['name' => 'Test', 'email' => $this->testEmail, 'password' => '12345678', 'password_confirmation' => '12345678']);
        $request->assertResponseStatus(201);
        $request->seeJson([
            'message' => 'USER_REGISTERED',
        ]);
    }

    /**
     * User Register Test with empty parameters
     */
    public function testRegisterFailsWithEmptyParameters()
    {
        //Clean Any Test User;
        User::where('email', $this->testEmail)->forceDelete();
        //This should fail with 422 and validation errors because name is empty
        $request = $this->post('/register', ['name' => '', 'email' => $this->testEmail, 'password' => '12345678', 'password_confirmation' => '12345678']);
        $request->assertResponseStatus(422);
        $request->seeJsonStructure(['name']);
    }

    /**
     * User Register Test with mismatched passwords
     */
    public function testRegisterFailsWithMismatchedPasswords()
    {
        //Clean Any Test User;
        User::where('email', $this->testEmail)->forceDelete();
        //This should fail with validation error because password confirmation mismatchs.
        $request = $this->post('/register', ['name' => 'Test', 'email' => $this->testEmail, 'password' => '12345678', 'password_confirmation' => '123456789']);
        $request->assertResponseStatus(422);
        $request->seeJsonStructure(['password']);
    }

    /**
     * User Register Test with a password that cant match with requirements
     */
    public function testRegisterFailsWithTooShortPasswords()
    {
        //Clean Any Test User;
        User::where('email', $this->testEmail)->forceDelete();
        //This should fail with validation error on password because password isnt match with requirements of minimum 8 character.
        $request = $this->post('/register', ['name' => 'Test', 'email' => $this->testEmail, 'password' => '123456', 'password_confirmation' => '123456']);
        $request->assertResponseStatus(422);
        $request->seeJsonStructure(['password']);
    }


    /**
     * User Register Test with already registered e-mail address
     */
    public function testRegisterFailsWithAlreadyRegisteredEmail()
    {
        //Clean Any Test User;
        User::where('email', $this->testEmail)->forceDelete();
        //Create User so we can expect collision on emails
        $this->createTestUser();
        //This should fail with 422 because there is an error validation on email
        $request = $this->post('/register', ['name' => '', 'email' => $this->testEmail, 'password' => '12345678', 'password_confirmation' => '12345678']);
        $request->assertResponseStatus(422);
        $request->seeJsonStructure(['email']);
    }

    /**
     * User Register Test with already malformed e-mail address
     */
    public function testRegisterFailsWithMalformedEmail()
    {
        //Clean Any Test User;
        User::where('email', $this->testEmail)->forceDelete();
        //This should fail with 422 because user forget @ in mail address
        $request = $this->post('/register', ['name' => '', 'email' => 'testtest.com', 'password' => '12345678', 'password_confirmation' => '12345678']);
        $request->assertResponseStatus(422);
        $request->seeJsonStructure(['email']);
    }

}
