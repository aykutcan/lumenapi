<?php

use App\Models\User;

abstract class TestCase extends Laravel\Lumen\Testing\TestCase
{
    /**
     * This is a unique email address we using for testing purposes
     * @var string
     */
    public $testEmail = 'test@test.com';

    /**
     * Creates the application.
     *
     * @return \Laravel\Lumen\Application
     */
    public function createApplication()
    {
        return require __DIR__.'/../bootstrap/app.php';
    }

    /**
     * Create an user with unique testings.
     * @return mixed
     */
    public function createTestUser()
    {
        return User::create(['name' => 'Test', 'email' => $this->testEmail, 'password' => app('hash')->make('12345678')]);
    }

}
