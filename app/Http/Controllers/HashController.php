<?php

namespace App\Http\Controllers;


use App\Http\Requests\RegisterRequest;
use App\Models\User;
use Illuminate\Http\Request;
use Auth;
use HashMaker\HashMakerFacade as HashGen;
use Faker\Factory;
use Log;
use Carbon\Carbon;
/**
 * #### Class HashController ####
 *
 * Generate hash and return it on demand
 *
 * @package App\Http\Controllers
 */
class HashController extends Controller
{
    /**
     * Generates hash and return it
     * @return \Illuminate\Http\JsonResponse
     */
    public function generateHash()
    {
        $faker = Factory::create();
        $text = $faker->text;
        $hash = HashGen::hash($text);
        Log::channel('hashlog')->info('Hash Created at '. Carbon::now() . ' : ' . $hash);
        return response()->json(['hash'=>$hash]);
    }

}
