<?php

namespace App\Http\Controllers;

use Laravel\Lumen\Routing\Controller as BaseController;
use Auth;

/**
 * #### Class Controller ####
 *
 * Base Class For All controllers
 *
 * @package App\Http\Controllers
 */
class Controller extends BaseController
{


    /**
     * Adds jwt tokens to response
     *
     *
     * @param $token
     * @return \Illuminate\Http\JsonResponse
     */
    protected function responseWithToken($token)
    {
        return response()->json([
            'access_token' => $token,
            'token_type' => 'bearer',
            'expires_in' => Auth::guard()->factory()->getTTL() * 60
        ]);
    }
}
