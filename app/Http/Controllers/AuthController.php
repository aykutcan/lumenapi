<?php

namespace App\Http\Controllers;


use App\Http\Requests\RegisterRequest;
use App\Models\User;
use Illuminate\Http\Request;
use Auth;

/**
 * #### Class AuthController ####
 *
 * Manages Login/ Registratiın procedures.
 *
 * @package App\Http\Controllers
 */
class AuthController extends Controller
{

    /**
     * Registers a new user with provided post data.
     *
     * Password requires to be minimum 8 characters.
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Validation\ValidationException
     * @throws \Exception
     */
    public function register(Request $request)
    {

        //Validating Request
        $this->validate($request, [
            'name' => 'required|string',
            'email' => 'required|email|unique:users',
            'password' => 'required|min:8|confirmed',
        ]);

        try {
            //Create User and return message
            $userData = [];
            $userData['name'] = $request->input('name');
            $userData['email'] = $request->input('email');
            $userData['password'] = app('hash')->make($request->input('password',''));

            $user = User::create($userData);
            if ($user) {
                //User Creation Successfull. Return info;
                return response()->json(['user' => $user, 'message' => 'USER_REGISTERED'], 201);
            } else {
                //User Creation Failed somehow. Return failure.
                return response()->json(['user' => $user, 'message' => 'USER_REGISTRATION_FAILED'], 400);
            }

        } catch (\Exception $e) {
            //We fail utterly. Return failure
            return response()->json(['message' => 'USER_REGISTRATION_FAILED'], 500);
        }

    }


    /**
     * Perform Login with provided login data.
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function login(Request $request)
    {
        //Validate Login Request
        $this->validate($request, [
            'email' => 'required|email|string',
            'password' => 'required|min:8|string',
        ]);
        $loginInfo = $request->only(['email', 'password']);
        //Attempt Login and return if fails.
        if (! $token = Auth::attempt($loginInfo)) {
            return response()->json(['message' => 'UNAUTHORIZED'], 401);
        }
        //Attempt successful return token
        return $this->responseWithToken($token);
    }


}
